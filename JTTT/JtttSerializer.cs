﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace JTTT
{
	public class JtttSerializer
	{
		public static void Serialize(object toSerialize)
		{
			var fileStream = new FileStream("DataFile.dat", FileMode.Create);
			var formatter = new BinaryFormatter();

			try
			{
				formatter.Serialize(fileStream, toSerialize);
			}
			finally
			{
				fileStream.Close();
			}
		}

		public static object Deserialize()
		{
			var fs = new FileStream("DataFile.dat", FileMode.Open);
			object deserialized;
			try
			{
				var formatter = new BinaryFormatter();

				deserialized =  formatter.Deserialize(fs);
			}
			finally
			{
				fs.Close();
			}
			return deserialized;
		}
	}
}
