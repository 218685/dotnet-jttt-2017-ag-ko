﻿using System;
using System.Net;
using System.Windows.Forms;
using JTTT.Actions;
using JTTT.Conditions;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

namespace JTTT
{
	public partial class MainForm : Form
	{
		private readonly Logger logger;
		private BindingList<Task> taskList;

		public MainForm()
		{
			InitializeComponent();
			logger = new Logger("jttt.log");
			taskList = LoadDataFromDb();
			TaskListBox.DataSource = taskList;
			TaskListBox.DisplayMember = "ListName";

			var conditionsList =
				new[] { typeof(ImageFoundByDescriptionCondition) };

			ConditionComboBox.DataSource = conditionsList;

			var actionsList =
				new[]
				{
					typeof(DownloadImageFromUriAction),
					typeof(MailImageFromUriAction)
				};
			ActionComboBox.DataSource = actionsList;
            
		}

        private BindingList<Task> LoadDataFromDb()
        {
            var list = new BindingList<Task>();

            var db = new JtttContext();   
            foreach (var g in db.Tasks)
            {                
                list.Add(g);
            }
	        return list;
        }
    
		private void StartButton_Click(object sender, EventArgs e)
		{
			CheckIfListIsEmpty();

			foreach (var task in taskList)
			{
				try
				{
					task.Run();
					MessageBox.Show(this, @"Wykonano akcję: " + task.JtttAction);
					logger.AddInfo("Wykonano akcję: " + task.JtttAction);
				}
				catch (WebException)
				{
					MessageBox.Show(this, @"Niewłaściwy adres Url!");
					logger.AddError("Niewłaściwy adres Url");
				}
				catch (InvalidOperationException)
				{
					MessageBox.Show(this, @"Nie znaleziono obrazka z podanym opisem!");
					logger.AddError("Nie znaleziono obrazka z podanym opisem");
				}
				catch (ArgumentNullException)
				{
					MessageBox.Show(this, @"Bład deserializacji!");
					logger.AddError("Błąd deserializacji");
				}
			}
		}

		private void CheckIfListIsEmpty()
		{
			if (!taskList.Any())
				MessageBox.Show(this, @"Nie ma żadnych zadań do wykonanania!");
		}

		private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void AddToListButton_Click(object sender, EventArgs e)
		{
            Type conditionType = null;
            Type actionType = null;
            string taskName = null;
            string pageUrl = null;
            string szukanyTekst = null;
            string email = null;
            try
			{
                GetTaskParameters(out conditionType, out actionType, out taskName);

				GetConditionParameters(out pageUrl, out szukanyTekst);
				CheckUserInputForNullOrEmpty(pageUrl, szukanyTekst);
				
				GetActionParameters(out email);
				CheckUserInputForNullOrEmpty(email);
				
				JtttCondition condition = null;
				if (conditionType == typeof(ImageFoundByDescriptionCondition))
				{
					condition = new ImageFoundByDescriptionCondition(pageUrl, szukanyTekst);
				}

				JtttAction action = null;
				if (actionType == typeof(MailImageFromUriAction))
				{
					action = new MailImageFromUriAction(email);
				}
				else if (actionType == typeof(DownloadImageFromUriAction))
				{
					action = new DownloadImageFromUriAction(szukanyTekst);
				}

				var zadanie = new Task(condition, action, taskName);
				taskList.Add(zadanie);
				logger.AddInfo("Dodano nowe zadanie: " + zadanie);

                using (var db = new JtttContext())
                {
                    db.Tasks.Add(zadanie);
                    db.SaveChanges();
                }
			}
			catch (ArgumentNullException)
			{
				MessageBox.Show(this, @"Nie podano wszystkich argumentów!");
				logger.AddError("Nie podano wszystkich argumentów");
			}
			catch (ArgumentException)
			{
				MessageBox.Show(this, @"Podano niepoprawne dane!");
				logger.AddError("Podano niepoprawne dane");
			}
		}

		private void GetTaskParameters(out Type conditionType, out Type actionType, out string taskName)
		{
			conditionType = (Type)ConditionComboBox.SelectedItem;
			logger.AddInfo("Odczytano typ warunku: " + ConditionComboBox.SelectedItem);

			actionType = (Type)ActionComboBox.SelectedItem;
			logger.AddInfo("Odczytano typ akcji: " + actionType);

			taskName = TaskNameTextBox.Text;
			logger.AddInfo("Odczytano nazwę zadania: " + taskName);
		}

		private void GetConditionParameters(out string pageUrl, out string szukanyTekst)
		{
			pageUrl = UrlTextBox.Text;
			logger.AddInfo("Odczytano ulr: " + pageUrl);

			szukanyTekst = SzukanyTekstTextBox.Text;
			logger.AddInfo("Odczytano szukany tekst: " + szukanyTekst);
		}

		private void CheckUserInputForNullOrEmpty(params string[] inputStrings)
		{
			foreach (var param in inputStrings)
			{
				if (string.IsNullOrEmpty(param))
					throw new ArgumentNullException(param);
			}
		}

		private void GetActionParameters(out string email)
		{
			email = EmailTextBox.Text;
			logger.AddInfo("Odczytano email: " + email);
		}

        private void ClearButton_Click(object sender, EventArgs e)
        {
            taskList.Clear();
        }
		private void ConditionComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void ActionComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (ActionComboBox.SelectedItem.Equals(typeof(MailImageFromUriAction)))
			{
				EmailTextBox.Visible = true;
				EmailLabel.Visible = true;
				mailDescriptionLabel.Visible = true;
			}
			else
			{
				EmailTextBox.Visible = false;
				EmailLabel.Visible = false;
				mailDescriptionLabel.Visible = false;
			}
		}

		private void SerializeButton_Click(object sender, EventArgs e)
		{
			CheckIfListIsEmpty();
			try
			{
				JtttSerializer.Serialize(taskList);
				MessageBox.Show(this, @"Serializacja pomyślna!");
			}
			catch (SerializationException ex)
			{
				MessageBox.Show(this, @"Błąd serializacji: " + ex.Message);
				logger.AddError("Błąd serializacji: " + ex.Message);
			}
		}

		private void DeserializeButton_Click(object sender, EventArgs e)
		{
			try
			{
				taskList = (BindingList<Task>)JtttSerializer.Deserialize();
				TaskListBox.DataSource = taskList;
				MessageBox.Show(this, @"Deserializacja pomyślna!");
			}
			catch (SerializationException ex)
			{
				MessageBox.Show(@"Błąd deserializacji: " + ex.Message);
				logger.AddError("Błąd deserializacji: " + ex.Message);
			}
		}

	}
}
