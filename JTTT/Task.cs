﻿using System;
using JTTT.Actions;
using JTTT.Conditions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JTTT
{
	[Serializable]
	public class Task
    {
        public int TaskId {get; set;}
        public string Name { get; set; }

        public virtual JtttCondition JtttCondition { get; set; }
        public virtual JtttAction JtttAction { get; set; }

        public Task()
	    {}

		public Task(JtttCondition condition, JtttAction action)
        {
	        JtttAction = action;
	        JtttCondition = condition;
        }

		public Task(JtttCondition condition, JtttAction action, string name)
		{
			JtttAction = action;
			JtttCondition = condition;
			Name = name;
		}

		public void Run()
        {
	        var checkValue = JtttCondition.Check();
	        JtttAction.Run(checkValue);
        }

	    public override string ToString()
	    {
		    return "Nazwa: " + Name + " | Warunek: " + JtttCondition.Description() + " | Akcja: " + JtttAction.Description();
	    }
    }

}
