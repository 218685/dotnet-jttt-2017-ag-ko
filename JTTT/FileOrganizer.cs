﻿using System;
using System.IO;

namespace JTTT
{
	internal class FileOrganizer
	{
		public static string GetFileExtensionFromUri(string fileUri)
		{
			var lastDotIndex = fileUri.LastIndexOf(".", StringComparison.Ordinal);
			var fileExtension = fileUri.Substring(lastDotIndex);
			return fileExtension;
		}

		public static void DeleteFileIfExists(string fileName)
		{
			if (File.Exists(fileName))
			{
				File.Delete(fileName);
			}
		}
	}
}
