﻿namespace JTTT
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.UrlTextBox = new System.Windows.Forms.TextBox();
			this.StartButton = new System.Windows.Forms.Button();
			this.SzukanyTekstTextBox = new System.Windows.Forms.TextBox();
			this.EmailTextBox = new System.Windows.Forms.TextBox();
			this.UrlLabel = new System.Windows.Forms.Label();
			this.SzukanyTekstLabel = new System.Windows.Forms.Label();
			this.EmailLabel = new System.Windows.Forms.Label();
			this.Opis = new System.Windows.Forms.Label();
			this.JezeliLabel = new System.Windows.Forms.Label();
			this.mailDescriptionLabel = new System.Windows.Forms.Label();
			this.DoThisLabel = new System.Windows.Forms.Label();
			this.ConditionComboBox = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.ActionComboBox = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.TaskListBox = new System.Windows.Forms.ListBox();
			this.ClearButton = new System.Windows.Forms.Button();
			this.DeserializeButton = new System.Windows.Forms.Button();
			this.SerializeButton = new System.Windows.Forms.Button();
			this.AddToListButton = new System.Windows.Forms.Button();
			this.TaskNameLabel = new System.Windows.Forms.Label();
			this.TaskNameTextBox = new System.Windows.Forms.TextBox();
			this.ListaLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// UrlTextBox
			// 
			this.UrlTextBox.Location = new System.Drawing.Point(115, 126);
			this.UrlTextBox.Name = "UrlTextBox";
			this.UrlTextBox.Size = new System.Drawing.Size(224, 20);
			this.UrlTextBox.TabIndex = 0;
			this.UrlTextBox.Text = "http://demotywatory.pl/";
			// 
			// StartButton
			// 
			this.StartButton.Location = new System.Drawing.Point(491, 276);
			this.StartButton.Name = "StartButton";
			this.StartButton.Size = new System.Drawing.Size(108, 64);
			this.StartButton.TabIndex = 1;
			this.StartButton.Text = "Start";
			this.StartButton.UseVisualStyleBackColor = true;
			this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// SzukanyTekstTextBox
			// 
			this.SzukanyTekstTextBox.Location = new System.Drawing.Point(115, 161);
			this.SzukanyTekstTextBox.Name = "SzukanyTekstTextBox";
			this.SzukanyTekstTextBox.Size = new System.Drawing.Size(224, 20);
			this.SzukanyTekstTextBox.TabIndex = 2;
			// 
			// EmailTextBox
			// 
			this.EmailTextBox.Location = new System.Drawing.Point(115, 313);
			this.EmailTextBox.Name = "EmailTextBox";
			this.EmailTextBox.Size = new System.Drawing.Size(224, 20);
			this.EmailTextBox.TabIndex = 3;
			this.EmailTextBox.Text = "arturgasinski@gmail.com";
			this.EmailTextBox.Visible = false;
			// 
			// UrlLabel
			// 
			this.UrlLabel.AutoSize = true;
			this.UrlLabel.Location = new System.Drawing.Point(71, 129);
			this.UrlLabel.Name = "UrlLabel";
			this.UrlLabel.Size = new System.Drawing.Size(29, 13);
			this.UrlLabel.TabIndex = 4;
			this.UrlLabel.Text = "URL";
			// 
			// SzukanyTekstLabel
			// 
			this.SzukanyTekstLabel.AutoSize = true;
			this.SzukanyTekstLabel.Location = new System.Drawing.Point(71, 164);
			this.SzukanyTekstLabel.Name = "SzukanyTekstLabel";
			this.SzukanyTekstLabel.Size = new System.Drawing.Size(34, 13);
			this.SzukanyTekstLabel.TabIndex = 5;
			this.SzukanyTekstLabel.Text = "Tekst";
			// 
			// EmailLabel
			// 
			this.EmailLabel.AutoSize = true;
			this.EmailLabel.Location = new System.Drawing.Point(48, 313);
			this.EmailLabel.Name = "EmailLabel";
			this.EmailLabel.Size = new System.Drawing.Size(61, 13);
			this.EmailLabel.TabIndex = 6;
			this.EmailLabel.Text = "Twój e-Mail";
			this.EmailLabel.Visible = false;
			// 
			// Opis
			// 
			this.Opis.AutoSize = true;
			this.Opis.Location = new System.Drawing.Point(44, 96);
			this.Opis.Name = "Opis";
			this.Opis.Size = new System.Drawing.Size(334, 13);
			this.Opis.TabIndex = 7;
			this.Opis.Text = "na podanej stronie znajduje się obrazek, którego podpis zawiera tekst";
			// 
			// JezeliLabel
			// 
			this.JezeliLabel.AutoSize = true;
			this.JezeliLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.JezeliLabel.Location = new System.Drawing.Point(34, 33);
			this.JezeliLabel.Name = "JezeliLabel";
			this.JezeliLabel.Size = new System.Drawing.Size(138, 35);
			this.JezeliLabel.TabIndex = 8;
			this.JezeliLabel.Text = "Jeżeli to:";
			// 
			// mailDescriptionLabel
			// 
			this.mailDescriptionLabel.AutoSize = true;
			this.mailDescriptionLabel.Location = new System.Drawing.Point(44, 281);
			this.mailDescriptionLabel.Name = "mailDescriptionLabel";
			this.mailDescriptionLabel.Size = new System.Drawing.Size(343, 13);
			this.mailDescriptionLabel.TabIndex = 9;
			this.mailDescriptionLabel.Text = "wyślij na podany adres e-mail wiadomość z tym obrazkiem w załączniku";
			this.mailDescriptionLabel.Visible = false;
			// 
			// DoThisLabel
			// 
			this.DoThisLabel.AutoSize = true;
			this.DoThisLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F);
			this.DoThisLabel.Location = new System.Drawing.Point(12, 214);
			this.DoThisLabel.Name = "DoThisLabel";
			this.DoThisLabel.Size = new System.Drawing.Size(169, 35);
			this.DoThisLabel.TabIndex = 10;
			this.DoThisLabel.Text = "wykonaj to:";
			// 
			// ConditionComboBox
			// 
			this.ConditionComboBox.FormattingEnabled = true;
			this.ConditionComboBox.Items.AddRange(new object[] {
            "Sprawdź opis obrazka na stronie"});
			this.ConditionComboBox.Location = new System.Drawing.Point(178, 47);
			this.ConditionComboBox.Name = "ConditionComboBox";
			this.ConditionComboBox.Size = new System.Drawing.Size(274, 21);
			this.ConditionComboBox.TabIndex = 11;
			this.ConditionComboBox.SelectedIndexChanged += new System.EventHandler(this.ConditionComboBox_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(175, 31);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(109, 13);
			this.label4.TabIndex = 12;
			this.label4.Text = "Wybierz typ warunku:";
			// 
			// ActionComboBox
			// 
			this.ActionComboBox.FormattingEnabled = true;
			this.ActionComboBox.Items.AddRange(new object[] {
            "Wyślij wiadomość email z obrazkiem",
            "Pobierz obrazek"});
			this.ActionComboBox.Location = new System.Drawing.Point(178, 228);
			this.ActionComboBox.Name = "ActionComboBox";
			this.ActionComboBox.Size = new System.Drawing.Size(274, 21);
			this.ActionComboBox.TabIndex = 13;
			this.ActionComboBox.SelectedIndexChanged += new System.EventHandler(this.ActionComboBox_SelectedIndexChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(175, 212);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(90, 13);
			this.label5.TabIndex = 14;
			this.label5.Text = "Wybierz typ akcji:";
			// 
			// TaskListBox
			// 
			this.TaskListBox.FormattingEnabled = true;
			this.TaskListBox.HorizontalScrollbar = true;
			this.TaskListBox.Location = new System.Drawing.Point(491, 71);
			this.TaskListBox.Name = "TaskListBox";
			this.TaskListBox.Size = new System.Drawing.Size(316, 199);
			this.TaskListBox.TabIndex = 15;
			this.TaskListBox.SelectedIndexChanged += new System.EventHandler(this.ListBox_SelectedIndexChanged);
			// 
			// ClearButton
			// 
			this.ClearButton.Location = new System.Drawing.Point(605, 276);
			this.ClearButton.Name = "ClearButton";
			this.ClearButton.Size = new System.Drawing.Size(99, 64);
			this.ClearButton.TabIndex = 16;
			this.ClearButton.Text = "Czyść";
			this.ClearButton.UseVisualStyleBackColor = true;
			this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
			// 
			// DeserializeButton
			// 
			this.DeserializeButton.Location = new System.Drawing.Point(710, 276);
			this.DeserializeButton.Name = "DeserializeButton";
			this.DeserializeButton.Size = new System.Drawing.Size(97, 31);
			this.DeserializeButton.TabIndex = 17;
			this.DeserializeButton.Text = "Deserializuj";
			this.DeserializeButton.UseVisualStyleBackColor = true;
			this.DeserializeButton.Click += new System.EventHandler(this.DeserializeButton_Click);
			// 
			// SerializeButton
			// 
			this.SerializeButton.Location = new System.Drawing.Point(710, 313);
			this.SerializeButton.Name = "SerializeButton";
			this.SerializeButton.Size = new System.Drawing.Size(97, 27);
			this.SerializeButton.TabIndex = 18;
			this.SerializeButton.Text = "Serializuj";
			this.SerializeButton.UseVisualStyleBackColor = true;
			this.SerializeButton.Click += new System.EventHandler(this.SerializeButton_Click);
			// 
			// AddToListButton
			// 
			this.AddToListButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.AddToListButton.Location = new System.Drawing.Point(115, 400);
			this.AddToListButton.Name = "AddToListButton";
			this.AddToListButton.Size = new System.Drawing.Size(163, 50);
			this.AddToListButton.TabIndex = 19;
			this.AddToListButton.Text = "Dodaj do listy";
			this.AddToListButton.UseVisualStyleBackColor = true;
			this.AddToListButton.Click += new System.EventHandler(this.AddToListButton_Click);
			// 
			// TaskNameLabel
			// 
			this.TaskNameLabel.AutoSize = true;
			this.TaskNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.TaskNameLabel.Location = new System.Drawing.Point(47, 372);
			this.TaskNameLabel.Name = "TaskNameLabel";
			this.TaskNameLabel.Size = new System.Drawing.Size(125, 20);
			this.TaskNameLabel.TabIndex = 20;
			this.TaskNameLabel.Text = "Nazwa zadania: ";
			// 
			// TaskNameTextBox
			// 
			this.TaskNameTextBox.Location = new System.Drawing.Point(178, 374);
			this.TaskNameTextBox.Name = "TaskNameTextBox";
			this.TaskNameTextBox.Size = new System.Drawing.Size(242, 20);
			this.TaskNameTextBox.TabIndex = 21;
			// 
			// ListaLabel
			// 
			this.ListaLabel.AutoSize = true;
			this.ListaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ListaLabel.Location = new System.Drawing.Point(485, 33);
			this.ListaLabel.Name = "ListaLabel";
			this.ListaLabel.Size = new System.Drawing.Size(173, 35);
			this.ListaLabel.TabIndex = 22;
			this.ListaLabel.Text = "Lista zadań";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(819, 460);
			this.Controls.Add(this.ListaLabel);
			this.Controls.Add(this.TaskNameTextBox);
			this.Controls.Add(this.TaskNameLabel);
			this.Controls.Add(this.AddToListButton);
			this.Controls.Add(this.SerializeButton);
			this.Controls.Add(this.DeserializeButton);
			this.Controls.Add(this.ClearButton);
			this.Controls.Add(this.TaskListBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.ActionComboBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.ConditionComboBox);
			this.Controls.Add(this.DoThisLabel);
			this.Controls.Add(this.mailDescriptionLabel);
			this.Controls.Add(this.JezeliLabel);
			this.Controls.Add(this.Opis);
			this.Controls.Add(this.EmailLabel);
			this.Controls.Add(this.SzukanyTekstLabel);
			this.Controls.Add(this.UrlLabel);
			this.Controls.Add(this.EmailTextBox);
			this.Controls.Add(this.SzukanyTekstTextBox);
			this.Controls.Add(this.StartButton);
			this.Controls.Add(this.UrlTextBox);
			this.Name = "MainForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox UrlTextBox;
		private System.Windows.Forms.Button StartButton;
		private System.Windows.Forms.TextBox SzukanyTekstTextBox;
		private System.Windows.Forms.TextBox EmailTextBox;
		private System.Windows.Forms.Label UrlLabel;
		private System.Windows.Forms.Label SzukanyTekstLabel;
		private System.Windows.Forms.Label EmailLabel;
		private System.Windows.Forms.Label Opis;
		private System.Windows.Forms.Label JezeliLabel;
		private System.Windows.Forms.Label mailDescriptionLabel;
        private System.Windows.Forms.Label DoThisLabel;
        private System.Windows.Forms.ComboBox ConditionComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ActionComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox TaskListBox;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button DeserializeButton;
        private System.Windows.Forms.Button SerializeButton;
        private System.Windows.Forms.Button AddToListButton;
        private System.Windows.Forms.Label TaskNameLabel;
        private System.Windows.Forms.TextBox TaskNameTextBox;
		private System.Windows.Forms.Label ListaLabel;
	}
}

