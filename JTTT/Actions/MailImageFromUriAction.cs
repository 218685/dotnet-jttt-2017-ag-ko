﻿using System;
using System.Net;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace JTTT.Actions
{
	[Serializable]
	public class MailImageFromUriAction : JtttAction
	{

		public string EmailAddress { get; set; }

		public MailImageFromUriAction()
		{
		}

		public MailImageFromUriAction(string emailAddress)
		{
			EmailAddress = emailAddress;
			FileName = "tmp_pic";
		}

		public MailImageFromUriAction(string fileName, string emailAddress)
		{
			FileName = fileName;
			EmailAddress = emailAddress;
		}

		public MailImageFromUriAction(string imageUri, string fileName, string emailAddress)
		{
			FileName = fileName;
			EmailAddress = emailAddress;
			this.ImageUri = imageUri;
		}

		public override void Run(params object[] actionParam)
		{
			if (actionParam != null)
				ImageUri = actionParam[0] as string;
			Run();
		}

		public override void Run()
		{
			DownloadRemoteImageFile();
			SendMailWithFileAttachment();
			FileOrganizer.DeleteFileIfExists(FileName);
		}

		private void DownloadRemoteImageFile()
		{
			var imageExtension = FileOrganizer.GetFileExtensionFromUri(ImageUri);
			FileName = FileName + imageExtension;
			using (var client = new WebClient())
			{
				client.DownloadFile(ImageUri, FileName);
			}
		}

		public void SendMailWithFileAttachment()
		{
			var message = CreateMessageWithAttachment();
			SendMailMessage(message);
		}

		private MimeMessage CreateMessageWithAttachment()
		{
			var message = new MimeMessage();

			message.From.Add(new MailboxAddress("JTTT", "dotnetjttt@gmail.com"));
			message.To.Add(new MailboxAddress("", EmailAddress));
			message.Subject = "Nowy obrazek";

			var builder = new BodyBuilder
			{
				TextBody = "Znaleźliśmy dla Ciebie nowy obraz, którego opis zawiera podane słowo."
			};

			builder.Attachments.Add(FileName);
			message.Body = builder.ToMessageBody();
			return message;
		}

		private static void SendMailMessage(MimeMessage message)
		{
			using (var client = new SmtpClient())
			{
				client.Connect("smtp.gmail.com", 465, SecureSocketOptions.SslOnConnect);
				client.Authenticate("dotnetjttt@gmail.com", "dotnethaslo");
				client.Send(message);
				client.Disconnect(true);
			}
		}

		public override string ToString()
		{
			return "Wyślij maila z wyszukanym obrazkiem";
		}

		public override string Description()
		{
			return GetDescription();
		}

		public static string GetDescription()
		{
			return "Wyślij maila z wyszukanym obrazkiem";
		}
	}
}
