﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JTTT.Actions
{
    [Serializable]
	public abstract class JtttAction
	{
        public int JtttActionId { get; set; }

        public string FileName { get; set; }
        [NotMapped]
        public string ImageUri { get; set; }
        

        public abstract void Run();
		public abstract void Run(params object[] actionParam);
		public abstract string Description();
	}
}
