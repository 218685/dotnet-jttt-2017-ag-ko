﻿using System;
using System.IO;
using System.Net;

namespace JTTT.Actions
{
	[Serializable]
	public class DownloadImageFromUriAction : JtttAction
	{
		public DownloadImageFromUriAction(){}

		public DownloadImageFromUriAction(string fileName)
		{
			FileName = fileName;
		}

		public DownloadImageFromUriAction(string imageUri, string fileName)
		{
			var imageExtension = FileOrganizer.GetFileExtensionFromUri(imageUri);
			FileName = fileName + imageExtension;
			ImageUri = imageUri;
		}

		private void DownloadRemoteImageFile()
		{
			var imageExtension = FileOrganizer.GetFileExtensionFromUri(ImageUri);
			FileName = FileName + imageExtension;

			Directory.CreateDirectory("download");
			var filePath = Path.Combine("download", FileName);
			using (var client = new WebClient())
			{
				client.DownloadFile(ImageUri, filePath);
			}
		}

		public override void Run()
		{
			DownloadRemoteImageFile();
		}

		public override void Run(params object[] actionParam)
		{
			if (actionParam != null)
				ImageUri = actionParam[0] as string;
			Run();
		}


		public override string ToString()
		{
			return "Pobierz obrazek na dysk";
		}

		public override string Description()
		{
			return GetDescription();
		}
		
		public static string GetDescription()
		{
			return "Pobierz obrazek na dysk";
		}
	}
}
