﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace JTTT.Conditions
{
	[Serializable]
	public class ImageFoundByDescriptionCondition : JtttCondition
	{
		public string PageUrl { get; set; }
		public string SzukanyTekst { get; set; }

		public ImageFoundByDescriptionCondition()
		{ }

		public ImageFoundByDescriptionCondition(string pageUrl, string szukanyTekst)
		{
			PageUrl = pageUrl;
			SzukanyTekst = szukanyTekst;
		}

		public override object Check()
		{
			var imageUrl = FindFirstImageUriWithMatchingDescription();
			return imageUrl;
		}

		private string FindFirstImageUriWithMatchingDescription()
		{
			var htmlDocument = new HtmlDocument();

			var pageHtml = GetPageHtml();
			htmlDocument.LoadHtml(pageHtml);

			var imageNode = htmlDocument.DocumentNode
				.Descendants("img")
				.First(x => x.GetAttributeValue("alt", "").Contains(SzukanyTekst, StringComparison.CurrentCultureIgnoreCase));

			var imageSourceUrl = imageNode.GetAttributeValue("src", "");

			return imageSourceUrl;
		}

		private string GetPageHtml()
		{
			using (var wc = new WebClient())
			{
				wc.Encoding = Encoding.UTF8;
				var html = WebUtility.HtmlDecode(wc.DownloadString(PageUrl));

				return html;
			}
		}

		public override string Description()
		{
			return GetDescription();
		}

		public override string ToString()
		{
			return "Sprawdź, czy na stronie jest obrazek z opisem";
		}

		public static string GetDescription()
		{
			return "Sprawdź, czy na stronie jest obrazek z opisem";
		}
	}
}
