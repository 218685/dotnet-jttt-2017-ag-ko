﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JTTT.Conditions
{
	[Serializable]
	public abstract class JtttCondition
	{
        public int JtttConditionId { get; set; }

        public abstract object Check();
		public abstract string Description();
	}
}
