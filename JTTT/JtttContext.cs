﻿using JTTT.Actions;
using JTTT.Conditions;
using System.Data.Entity;

namespace JTTT
{
   public class JtttContext: DbContext
    {
        public JtttContext(): base("JtttAplication_AG_KO")
        {}

        public DbSet<JtttCondition> JtttConditions { get; set; }
        public DbSet<JtttAction> JtttActions { get; set; }
        public DbSet<Task> Tasks { get; set; }
    }
}
