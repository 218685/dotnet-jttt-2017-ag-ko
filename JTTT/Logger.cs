﻿using System;
using System.IO;

namespace JTTT
{
	internal class Logger
	{
		public string FileName { get; }

		public Logger(string fileName)
		{
			FileName = fileName;
		}

		public void ClearFileContents()
		{
			using (var writer = new StreamWriter(FileName))
			{
				writer.WriteLine(DateTime.Now + " | Rozpoczęto nową sesję");
			}
		}

		public void AddEntry(string logEntry)
		{
			using (var writer = new StreamWriter(FileName,true))
			{
				writer.WriteLine(DateTime.Now + " | " + logEntry);
			}
		}

		public void AddInfo(string infoLogEntry)
		{
			using (var writer = new StreamWriter(FileName, true))
			{
				writer.WriteLine(DateTime.Now + " | INFO: " + infoLogEntry);
			}
		}

		public void AddError(string errorLogEntry)
		{
			using (var writer = new StreamWriter(FileName, true))
			{
				writer.WriteLine(DateTime.Now + " | ERROR: " + errorLogEntry);
			}
		}
	}
}
